#include <Arduino.h>

unsigned long previousMillis = 0;        // will store last time LED was updated


const long interval = 1000;           // interval at which to blink (milliseconds)
// set boolean flag to change the state of the pin
bool flag = false;
void setup() {

  Serial.begin(9600);
}

void loop() {
  unsigned long currentMillis = millis();
  //Serial.println("the current time is:");
  //Serial.println(currentMillis);

  if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;

    //Serial.println("the previous time is:");
    //Serial.println(previousMillis);

    //conditions to change the state of the boolean flag
    if(flag==false){
      flag=true;
      Serial.println("on");
      // changes state of pin 13 to be on
      PORTB=B00100000;
  }
    else  if (flag==true){
        flag=false;
        Serial.println("off");
        //changes state of pin 13 on be on
        PORTB=B00000000;
      }

}
}
